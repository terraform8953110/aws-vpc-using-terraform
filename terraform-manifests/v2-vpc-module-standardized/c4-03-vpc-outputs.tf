# VPC Output Values

# Output: VPC ID
output "vpc_id" {
  description = "The ID of the created VPC"
  value       = module.vpc.vpc_id
}

# Output: VPC CIDR blocks
output "vpc_cidr_block" {
  description = "The CIDR block of the created VPC"
  value       = module.vpc.vpc_cidr_block
}

# Output: VPC Private Subnets
output "private_subnets" {
  description = "List of private subnets created in the VPC"
  value       = module.vpc.private_subnets
}

# Output: VPC Public Subnets
output "public_subnets" {
  description = "List of public subnets created in the VPC"
  value       = module.vpc.public_subnets
}

# Output: NAT Public IPs
output "nat_public_ips" {
  description = "List of public IP addresses assigned to NAT gateways"
  value       = module.vpc.nat_public_ips
}

# Output: Availability Zones
output "azs" {
  description = "List of availability zones where the VPC resources are deployed"
  value       = module.vpc.azs
}
