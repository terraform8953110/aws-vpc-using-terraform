# VPC Input Variables

# Variable: VPC Name
variable "vpc_name" {
  description = "The name of the VPC"
  type        = string
  default     = "myvpc"
}

# Variable: VPC CIDR Block
variable "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

# Variable: VPC Availability Zones
variable "vpc_avaibality_zones" {
  description = "The availability zones for the VPC"
  type        = list(string)
  default     = ["us-east-1a", "us-east-1b"]
}

# Variable: VPC Public Subnets
variable "vpc_public_subnets" {
  description = "List of CIDR blocks for public subnets"
  default     = ["10.0.101.0/24", "10.0.102.0/24"]
}

# Variable: VPC Private Subnets
variable "vpc_private_subnets" {
  description = "List of CIDR blocks for private subnets"
  default     = ["10.0.1.0/24", "10.0.2.0/24"]
}

# Variable: VPC Database Subnets
variable "vpc_database_subnet" {
  description = "List of CIDR blocks for database subnets"
  default     = ["10.0.151.0/24", "10.0.152.0/24"]
}

# Variable: Create Database Subnet Group
variable "create_database_subnet_group" {
  description = "Whether to create a database subnet group"
  type        = bool
  default     = true
}

# Variable: Create Database Subnet Route Table
variable "vpc_create_database_subnet_route_table" {
  description = "Whether to create a route table for database subnets"
  type        = bool
  default     = true
}

# Variable: Enable NAT Gateway
variable "vpc_enable_nat_gateway" {
  description = "Whether to enable NAT gateway for outbound communication"
  type        = bool
  default     = true
}

# Variable: Single NAT Gateway
variable "vpc_single_nat_gateway" {
  description = "Whether to use a single NAT gateway for outbound communication"
  type        = bool
  default     = true
}

# Variable: Enable DNS Hostnames
variable "vpc_enable_dns_hostnames" {
  description = "Whether to enable DNS hostnames for the VPC"
  type        = bool
  default     = true
}

# Variable: Enable DNS Support
variable "vpc_enable_dns_support" {
  description = "Whether to enable DNS support for the VPC"
  type        = bool
  default     = true
}
